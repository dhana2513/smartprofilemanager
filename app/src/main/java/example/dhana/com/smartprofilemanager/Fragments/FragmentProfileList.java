package example.dhana.com.smartprofilemanager.Fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import example.dhana.com.smartprofilemanager.R;
import example.dhana.com.smartprofilemanager.Adapters.ViewPagerAdapter;


public class FragmentProfileList extends Fragment {

    @SuppressLint("ResourceAsColor")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.lay_frag_profile_list, null);

        TabLayout tabLayout = view.findViewById(R.id.tabLayout);

        ViewPager viewPager = view.findViewById(R.id.viewPager);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        viewPagerAdapter.addFragment(new FragProfileListByTime(), "Time");
        viewPagerAdapter.addFragment(new FragProfileListByLocation(), "Location");

        viewPager.setAdapter(viewPagerAdapter);

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(R.color.white);


        return view;
    }

}
