package example.dhana.com.smartprofilemanager.POJO;


import java.io.Serializable;

public class Profile implements Serializable {

    private int ID;
    private String NAME;
    private String MODE;
    private int RINGER_VOLUME;
    private int MEDIA_VOLUME;
    private int ALARM_VOLUME;
    private int VOICE_VOLUME;
    private int NOTIFICATION_VOLUME;
    private int SYSTEM_VOLUME;
    private String REMINDER;
    private String STARTTIME;
    private String ENDTIME;
    private String LOCATION_LAT;
    private String LOCATION_LONG;
    private String REPEAT;
    private int STATUS;

    public Profile(int ID, String NAME, String MODE, int RINGER_VOLUME, int MEDIA_VOLUME, int ALARM_VOLUME, int VOICE_VOLUME, int NOTIFICATION_VOLUME, int SYSTEM_VOLUME, String REMINDER, String STARTTIME, String ENDTIME, String LOCATION_LAT, String LOCATION_LONG, String REPEAT, int STATUS) {
        this.ID = ID;
        this.NAME = NAME;
        this.MODE = MODE;
        this.RINGER_VOLUME = RINGER_VOLUME;
        this.MEDIA_VOLUME = MEDIA_VOLUME;
        this.ALARM_VOLUME = ALARM_VOLUME;
        this.VOICE_VOLUME = VOICE_VOLUME;
        this.NOTIFICATION_VOLUME = NOTIFICATION_VOLUME;
        this.SYSTEM_VOLUME = SYSTEM_VOLUME;
        this.REMINDER = REMINDER;
        this.STARTTIME = STARTTIME;
        this.ENDTIME = ENDTIME;
        this.LOCATION_LAT = LOCATION_LAT;
        this.LOCATION_LONG = LOCATION_LONG;
        this.REPEAT = REPEAT;
        this.STATUS = STATUS;
    }

    public int getID() {
        return ID;
    }

    public String getNAME() {
        return NAME;
    }

    public String getMODE() {
        return MODE;
    }

    public int getRINGER_VOLUME() {
        return RINGER_VOLUME;
    }

    public int getMEDIA_VOLUME() {
        return MEDIA_VOLUME;
    }

    public int getALARM_VOLUME() {
        return ALARM_VOLUME;
    }

    public int getVOICE_VOLUME() {
        return VOICE_VOLUME;
    }

    public int getNOTIFICATION_VOLUME() {
        return NOTIFICATION_VOLUME;
    }

    public int getSYSTEM_VOLUME() {
        return SYSTEM_VOLUME;
    }

    public String getREMINDER() {
        return REMINDER;
    }

    public String getSTARTTIME() {
        return STARTTIME;
    }

    public String getENDTIME() {
        return ENDTIME;
    }

    public String getLOCATION_LAT() {
        return LOCATION_LAT;
    }

    public String getLOCATION_LONG() {
        return LOCATION_LONG;
    }

    public String getREPEAT() {
        return REPEAT;
    }

    public int getSTATUS() {
        return STATUS;
    }
}
