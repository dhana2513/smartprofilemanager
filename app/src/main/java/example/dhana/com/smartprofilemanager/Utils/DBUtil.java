package example.dhana.com.smartprofilemanager.Utils;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import example.dhana.com.smartprofilemanager.POJO.Profile;

public class DBUtil {
    private static DBUtil dbUtil;
    private SQLiteDatabase mDbProfiles;
    private final String mTableName = "PROFILES";

    private DBUtil(Context context) {
        mDbProfiles = new ProfileHelper(context,"DBProfile",null,1).getWritableDatabase();
    }

    public static DBUtil getInstance( Context context ){

        if( dbUtil == null ){
            dbUtil = new DBUtil(context);
        }
        return dbUtil;
    }

    public ArrayList<Profile> getProfileList(){
        ArrayList<Profile> listProfiles = new ArrayList<>();

        @SuppressLint("Recycle")
        Cursor cursor = mDbProfiles.rawQuery("SELECT * FROM "+ mTableName,null );

        while( cursor.moveToNext() ){

            int id = cursor.getInt(1);
            String name = cursor.getString(2);
            String mode = cursor.getString(3);
            int ringerVolume = cursor.getInt(4);
            int mediaVolume = cursor.getInt(5);
            int alarmVolume = cursor.getInt(6);
            int voiceVolume = cursor.getInt(7);
            int notificationVolume = cursor.getInt(8);
            int systemVolume = cursor.getInt(9);
            String reminder = cursor.getString(10);
            String startTime = cursor.getString(11);
            String endTime = cursor.getString(12);
            String locationLat = cursor.getString(13);
            String locationLong = cursor.getString(14);
            String repeat = cursor.getString(15);
            int status = cursor.getInt(16);

            Profile profile = new Profile(id, name ,mode, ringerVolume,mediaVolume,alarmVolume,voiceVolume,notificationVolume,systemVolume,
                    reminder,startTime,endTime,locationLat,locationLong,repeat,status);

            listProfiles.add( profile );
        }

        return listProfiles;
    }

    public void insertProfile(Profile profile){

        ContentValues values = new ContentValues();
        values.put("ID", profile.getID() );
        values.put("NAME", profile.getNAME() );
        values.put("MODE", profile.getMODE() );
        values.put("RINGER_VOLUME", profile.getRINGER_VOLUME() );
        values.put("MEDIA_VOLUME", profile.getMEDIA_VOLUME() );
        values.put("ALARM_VOLUME", profile.getALARM_VOLUME() );
        values.put("VOICE_VOLUME", profile.getVOICE_VOLUME() );
        values.put("NOTIFICATION_VOLUME", profile.getNOTIFICATION_VOLUME() );
        values.put("SYSTEM_VOLUME", profile.getSYSTEM_VOLUME() );
        values.put("REMINDER", profile.getREMINDER() );
        values.put("STARTTIME", profile.getSTARTTIME() );
        values.put("ENDTIME", profile.getENDTIME() );
        values.put("LOCATION_LAT", profile.getLOCATION_LAT() );
        values.put("LOCATION_LONG", profile.getLOCATION_LONG() );
        values.put("REPEAT", profile.getREPEAT() );
        values.put("STATUS", profile.getSTATUS() );

        mDbProfiles.insert( mTableName, null, values );

    }

    private class ProfileHelper extends SQLiteOpenHelper{

        ProfileHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {

            sqLiteDatabase.execSQL(" CREATE TABLE IF NOT EXISTS "+mTableName+"(ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, MODE TEXT,"+
            " RINGER_VOLUME INTEGER, MEDIA_VOLUME INTEGER, ALARM_VOLUME INTEGER, VOICE_VOLUME INTEGER, NOTIFICATION_VOLUME INTEGER, SYSTEM_VOLUME INTEGER," +
                    "REMINDER TEXT, STARTTIME TEXT, ENDTIME TEXT,LAT TEXT, LONG TEXT, REPEAT TEXT, STATUS INTEGER)");
       }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        }
    }
}
