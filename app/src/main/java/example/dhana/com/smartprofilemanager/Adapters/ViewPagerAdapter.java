package example.dhana.com.smartprofilemanager.Adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private ArrayList<Fragment> mListFragments;
    private ArrayList<String> mListTitles;

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
        mListFragments = new ArrayList<>();
        mListTitles = new ArrayList<>();
    }

    public void addFragment(Fragment fragment, String title){
        mListTitles.add( title);
        mListFragments.add( fragment );
    }
    @Override
    public Fragment getItem(int i) {
        return mListFragments.get(i);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mListTitles.get( position );
    }

    @Override
    public int getCount() {
        return mListTitles.size();
    }
}
