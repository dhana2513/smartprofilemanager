package example.dhana.com.smartprofilemanager.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import example.dhana.com.smartprofilemanager.R;

public class FragProfileListByTime extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lay_frag_profile_list_by_location, null);
        RecyclerView recyclerViewByTime = view.findViewById( R.id.recyclerViewByTime );

        return view;

    }
}