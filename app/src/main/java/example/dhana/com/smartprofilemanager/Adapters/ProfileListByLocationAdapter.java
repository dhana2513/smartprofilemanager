package example.dhana.com.smartprofilemanager.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

public class ProfileListByLocationAdapter extends RecyclerView.Adapter<ProfileListByLocationAdapter.ProfileHolder> {

    @NonNull
    @Override
    public ProfileHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileHolder profileHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ProfileHolder extends RecyclerView.ViewHolder{
        public ProfileHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
